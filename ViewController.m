//
//  ViewController.m
//  Labb1
//
//  Created by Ebba on 2016-01-26.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *colorSwitch;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.colorSwitch.on = NO;
    self.textLabel.text = @"Visa Ebbas favoritfärg.";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showNewColor:(UISwitch *)sender {
    
    if(self.colorSwitch.isOn){
        self.view.backgroundColor = [UIColor redColor];
        self.textLabel.text = @"Back to black.";
    } else {
        self.view.backgroundColor = [UIColor blackColor];
        self.textLabel.text = @"Visa Ebbas favoritfärg.";
    }
    
}

@end
